package com.testautothon.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class YouTubueHomePage extends BasePage {

    private WebDriver driver;
    /**
     * Constructor to initialize instance variables and verify if current page is home Page.
     *
     * @param serviceUrl (service url)
     * @param driver Web Driver
     */
    public YouTubueHomePage(final String serviceUrl, WebDriver driver) {
        driver.get(serviceUrl);
        this.driver = driver;
        Assert.assertTrue(this.driver.getTitle().contains(""),
                "Title does not contain home page text. Title of the page:" + driver.getTitle());
        PageFactory.initElements(this.driver, this);
    }

    /**
     * This method search the query in the youtube homepage
     *
     * @param searchQuery - query to search
     * @param isSearchCompleted - returns true if search is successful
     * @return true if search is successful
     */
    public boolean search(String searchQuery, boolean isSearchCompleted) {
        WebElement searchBoxEle = driver.findElement(By.cssSelector("ytd-searchbox[id='search']"));
        searchBoxEle.findElement(By.cssSelector("input[id='search']")).sendKeys(searchQuery);
        WebElement searchButton = driver.findElement(By.cssSelector("button[id='search-icon-legacy']"));
        searchButton.click();

        if(!isSearchCompleted) {
            if(this.driver.getCurrentUrl().contains("search_query=step-inforum")) {
                return false;
            }
        }
        return true;
    }

    /**
     * This method click on the queried you tube channel
     *
     */
    public YouTubeChannelPage clickOnChannel() {
        List<WebElement> lstChannels = driver.findElements(By.tagName("ytd-channel-renderer"));
        for (WebElement ele : lstChannels) {
            if(ele.getText().equals("STeP-IN Forum")) {
                getParentElement(ele, 2).click();
                break;
            }
        }
        return  new YouTubeChannelPage(this.driver);
    }
}