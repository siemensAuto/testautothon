package com.testautothon.pages;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UpNextVideosRequest {
    private String team;
    private String video;
    @SerializedName("upcoming-videos")
    private List<String> upcomingVideos;

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public List<String> getUpcomingVideos() {
        return upcomingVideos;
    }

    public void setUpcomingVideos(List<String> upcomingVideos) {
        this.upcomingVideos = upcomingVideos;
    }
}
