package com.testautothon.pages;

import com.testautothon.RestCall.ApiRestCall;
import org.openqa.selenium.*;
import org.testng.Assert;

import java.util.List;

public class YouTubeChannelPage extends BasePage{
    private WebDriver driver;
    /**
     * Constructor to initialize instance variables and verify if current page is home Page.
     *
     * @param driver - WebDriver
     */
    public YouTubeChannelPage(WebDriver driver) {
        this.driver = driver;
        Assert.assertTrue(this.driver.getTitle().contains(""),
                "Title does not contain home page text. Title of the page:" + driver.getTitle());
        //PageFactory.initElements(this.driver, this);
    }

    /**
     * This method will click and verify if videos tab is selected
     *
     * @param isVideoTabSelected - returns true if videos tab is selected
     * @return true if videos tab is selected
     */
    public boolean clickVideosTab(boolean isVideoTabSelected) {
        List<WebElement> lstTabs = driver.findElements(By.cssSelector("div[id='tabsContent']"));
        for(WebElement ele : lstTabs) {
            if(ele.getText().equals("Videos")) {
                getParentElement(ele, 1).click();
            }
        }
        if(!isVideoTabSelected) {
            if(this.driver.getCurrentUrl().contains("stepinforum/videos")) {
                return false;
            }
        }
        return true;
    }

    public String getVideoName(){
    String videoName =  ApiRestCall.getAsString("http://54.169.34.162:5252/video","");
    return videoName;
    }

    public void scrollToCentre(String input) {
        Dimension initial_size = driver.manage().window().getSize();
        int height = initial_size.getHeight();
        System.out.println("height " + height);
        int width = initial_size.getWidth();
        System.out.println("width " + width);

        JavascriptExecutor js = (JavascriptExecutor) driver;

        WebElement ele = driver.findElement(By.xpath("//a[contains(text()," + input + "]"));
        ele = getParentElement(ele,4);
        ele.findElement(By.xpath("//*[@id=\"img\"]"));
        js.executeScript("arguments[0].scrollIntoView(true);", ele);

        Point location = ele.getLocation();
        int h = height/2;
        System.out.println(location.getY());

        if(location.getY()<h )
            js.executeScript("window.scrollBy(0,"+height / 2+")", ele);
        else
            js.executeScript("window.scrollBy(0,-"+height / 2+")", ele);
    }
}
