package com.testautothon.pages;

import com.google.gson.Gson;
import com.testautothon.RestCall.ApiRestCall;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class YouTubeVideoPlayerlPage extends BasePage {
    private WebDriver driver;

    /**
     * Constructor to initialize instance variables and verify if current page is home Page.
     *
     * @param driver - WebDriver
     */
    public YouTubeVideoPlayerlPage(WebDriver driver) {
        this.driver = driver;
        Assert.assertTrue(this.driver.getTitle().contains(""),
                "Title does not contain home page text. Title of the page:" + driver.getTitle());
        //PageFactory.initElements(this.driver, this);
    }

    public void selectOptionFromPlayerSettings(String option) {
        try {
            clickOnSettingsButton();
            WebElement options = driver.findElement(By.cssSelector("div[class='ytp-popup ytp-settings-menu'"));
            List<WebElement> opts = options.findElements(By.cssSelector("div[class='ytp-menuitem']"));
            for (WebElement opt : opts) {
                if (opt.findElement(By.cssSelector("div[class='ytp-menuitem-label']")).getText().equals(option)) {
                    opt.click();
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clickOnSettingsButton() {
        try {
            WebElement settings = driver.findElement(By.cssSelector("button[class='ytp-button ytp-settings-button ytp-hd-quality-badge']"));
            settings.click();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<String> getListOfUpNextVideos() {
        List<String> upNextVideos = new ArrayList<>();
        try {
            WebElement upNextParent = driver.findElement(By.tagName("ytd-watch-next-secondary-results-renderer"));
            List<WebElement> upNextList = upNextParent.findElements(By.tagName("ytd-compact-video-renderer"));

            for (int videoNo = 0; videoNo < 10; videoNo++) {
                String videoName = upNextList.get(videoNo).findElement(By.cssSelector("span[id='video-title']")).getText();
                upNextVideos.add(videoName);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return upNextVideos;
    }

    public void postUpNextVideos(List<String> videos) {
        try {
            UpNextVideosRequest upNextVideosRequest = new UpNextVideosRequest();
            upNextVideosRequest.setTeam("Siemens-Healthineers");
            upNextVideosRequest.setVideo("");
            upNextVideosRequest.setUpcomingVideos(videos);
            postResults(upNextVideosRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String postResults(UpNextVideosRequest upNextVideosRequest) {
        String UUID = "";
        try {
            JSONObject jsonObject = new JSONObject(new Gson().toJson(upNextVideosRequest));
            ApiRestCall.post("", jsonObject);
            File file = new File(System.getProperty("user.dir")+"/results.json");
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(new Gson().toJson(upNextVideosRequest));
            fileWriter.flush();
            UUID = ApiRestCall.formDataPost(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return UUID;
    }

}
