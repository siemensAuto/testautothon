package com.testautothon.pages;

import com.testautothon.driver.DriverFactory;
import org.jsoup.Connection;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import javax.annotation.Nonnull;

public class BasePage {



    public BasePage(){

    }

    public BasePage(WebDriver driver) {
//        this.driver = driver;
//        PageFactory.initElements(driver, this);
    }
    /**
     * Returns parent DOM object of given WebElement.
     *
     * @param obj         - child object of wanted parent.
     * @param levelsAbove - Parent level.
     * @return Parent object
     */
    public static WebElement getParentElement(@Nonnull WebElement obj, int levelsAbove)
    {
        WebElement parentEle = obj;
        try
        {
            for (int i = 0; i < levelsAbove; i++)
            {
                parentEle = parentEle.findElement(By.xpath(""));
            }

        }
        catch (NullPointerException e)
        {
            //Reporter.("utils.TcUtils.getParentElement: Object ist null, no parent exists!");
        }
        catch (Exception e)
        {
            //TcLog.error("utils.TcUtils.getParentElement: ", e);
        }
        return parentEle;
    }
}
