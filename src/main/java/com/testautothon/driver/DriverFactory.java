package com.testautothon.driver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * DriverFactory class to create WebDriver instance to run automated tests against desktop browsers, device browser and android app.
 **/
public class DriverFactory {
	protected WebDriver driver;
	protected ExtentReports extent;
	protected ExtentTest test;

	/* Starting Appium from Console */
	/*AppiumDriverLocalService service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder().
			usingDriverExecutable(new File("C:/Program Files/nodejs/node.exe"))
					.withAppiumJS(new File("C:/Program Files/Appium/resources/app/node_modules/appium/build/lib/appium.js")));

	
	*//* For Device Only - START APPIUM SERVER *//*
	protected void appiumStart() {
		if (service.isRunning() == true) {
			service.stop();
			service.start();
		} else {
			service.start();
		}
	}

	*//* For Device Only - STOP APPIUM SERVER *//*
	protected void appiumStop() throws IOException {
		service.stop();
	}*/

	/* Properties to trigger Chrome browser*/
	protected void chromeDriver(String...mobilePhone ) throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver/chromedriver.exe");
		if(mobilePhone.length>0) {
			Map<String, String> mobileEmulation = new HashMap<>();
			mobileEmulation.put("deviceName", mobilePhone[0]);
			ChromeOptions chromeOptions = new ChromeOptions();
			chromeOptions.setExperimentalOption("mobileEmulation", mobileEmulation);
			driver = new ChromeDriver(chromeOptions);
		}
		else
		{
            ChromeOptions options = new ChromeOptions();
            options.addArguments("start-maximized");
            options.addArguments("disable-infobars");
            options.addArguments("--dom-automation");
            options.addArguments("--disable-web-security");
            options.addArguments("--safebrowsing-disable-download-protection");
            options.addArguments("--incognito");
            driver = new ChromeDriver(options);
		}
		//String nodeUrl = "http://10.83.33.10:4444/wd/hub";
		//DesiredCapabilities capability = new DesiredCapabilities();
		//capability.setBrowserName("chrome");
		//capability.setPlatform(Platform.WIN10);
		//driver = new RemoteWebDriver(new URL(nodeUrl), capability);
		driver.manage().window().maximize();
	}

	/* Properties to trigger Firefox browser*/
	protected void firefoxDriver() throws MalformedURLException {
		System.setProperty("webdriver.gecko.driver", "./src/main/resources/geckodriver/geckodriver.exe");
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("start-maximized");
		options.addArguments("disable-infobars");
		options.addArguments("--dom-automation");
		options.addArguments("--disable-web-security");
		options.addArguments("--safebrowsing-disable-download-protection");
		options.addArguments("--incognito");
		driver = new FirefoxDriver(options);
		//String nodeUrl = "http://10.83.33.10:4444/wd/hub";
		//DesiredCapabilities capability = new DesiredCapabilities();
		//capability.setBrowserName("firefox");
		//driver = new RemoteWebDriver(new URL(nodeUrl), capability);
		driver.manage().window().maximize();
	}

	/* Properties to trigger IE browser*/
	protected void ieDriver() throws MalformedURLException {
		DesiredCapabilities capabilities = new DesiredCapabilities();

		capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
		capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
		capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
		capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
		capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
		System.setProperty("webdriver.ie.driver","./src/main/resources/iedriver/IEDriverServer.exe");
		driver = new InternetExplorerDriver(capabilities);
		driver.manage().window().maximize();
	}

	/**
	 * This method invokes a standalone browser
	 * 
	 **/
	protected WebDriver invokeBrowser(String browser, String...mobilePhone) throws MalformedURLException {
		if (browser.equalsIgnoreCase("firefox")) {
			firefoxDriver();
			return driver;
		}

		else if (browser.equalsIgnoreCase("chrome")) {

			if(mobilePhone.length >0 )
			{
				chromeDriver(mobilePhone);
			}
			else
			{
				chromeDriver();
			}
			return driver;
		}

		else if (browser.equalsIgnoreCase("internetexplorer")) {
			ieDriver();
			return driver;
		}
		return driver;
	}

	/**
	 * This method invokes a grid browser
	 * 
	 **/
	protected static WebDriver invokeBrowserInGrid(String browser) throws MalformedURLException {
		String nodeURL = null;
		WebDriver driver = null;
		if (browser.equalsIgnoreCase("firefox")) {
			nodeURL = DriverManager.getNodeUrl1();
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setPlatform(Platform.WINDOWS);
			driver = new RemoteWebDriver(new URL(nodeURL), capabilities);
			driver.manage().window().maximize();
			return driver;
		}

		else if (browser.equalsIgnoreCase("chrome")) {
			nodeURL = DriverManager.getNodeUrl2();
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setPlatform(Platform.WINDOWS);
			driver = new RemoteWebDriver(new URL(nodeURL), capabilities);
			driver.manage().window().maximize();
			return driver;
		}

		else if (browser.equalsIgnoreCase("internetexplorer")) {
			nodeURL = DriverManager.getNodeUrl3();
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			capabilities.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, false);
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			capabilities.setPlatform(Platform.WINDOWS);
			driver = new RemoteWebDriver(new URL(nodeURL), capabilities);
			driver.manage().window().maximize();
			return driver;
		}

		else {
			nodeURL = DriverManager.getNodeUrl2();
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setPlatform(Platform.WINDOWS);
			driver = new RemoteWebDriver(new URL(nodeURL), capabilities);
			driver.manage().window().maximize();
			return driver;
		}
	}

	/**
	 * This method closes the browser
	 **/
	protected void closeBrowser() throws IOException {
		driver.quit();
	}
	
	/**
	 * This method runs scripts in a device using grid
	 * @throws MalformedURLException 
	 **/
	protected WebDriver setupInGrid(String browser) throws MalformedURLException {
		String nodeURL = null;
		WebDriver driver = null;
		nodeURL = DriverManager.getDeviceNodeUrl();
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "6.0.1");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus");
		driver = new RemoteWebDriver(new URL(nodeURL), capabilities);
		return driver;
	}

	/**
	 * This method runs scripts in a device browser
	 **/
	@SuppressWarnings("rawtypes")
	protected WebDriver setup(String browser) throws MalformedURLException, InterruptedException {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("automationName", DriverManager.getAutomationName());
		capabilities.setCapability("deviceName", DriverManager.getDeviceName());
		capabilities.setCapability("platformName", DriverManager.getPlatformName());
		capabilities.setCapability("platformVersion", DriverManager.getPlatformVersion());
		capabilities.setCapability("app", DriverManager.getApp());
		capabilities.setCapability("device", DriverManager.getDevice());
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
		return driver;
	}

	/**
	 * This method runs scripts in a mobile application
	 **/
	@SuppressWarnings("rawtypes")
	protected WebDriver setupApp(String browser) throws MalformedURLException, InterruptedException {
		File app = new File("./src/test/resources/apk/FormApp.apk");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("appium-version", "1.4.16.1");
		capabilities.setCapability("automationName", DriverManager.getAutomationName());
		capabilities.setCapability("deviceName", DriverManager.getDeviceName());
		capabilities.setCapability("platformName", DriverManager.getPlatformName());
		capabilities.setCapability("platformVersion",DriverManager.getPlatformVersion());
		capabilities.setCapability("device", DriverManager.getDevice());
		capabilities.setCapability("app", app.getAbsolutePath());
		capabilities.setCapability("noReset", DriverManager.getNoReset());
		capabilities.setCapability("fullReset", DriverManager.getFullReset());
		capabilities.setCapability("appActivity", "com.anuj.task1.FormLogin");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);
		return driver;
	}
}
