package com.testautothon.RestCall;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.testing.http.apache.MockHttpClient;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;

public class ApiRestCall {


    public ApiRestCall() {
    }

    public static HttpGetResponse get(String url, String authToken, Boolean... ignoreWait) {
        HttpGetResponse getResponse = null;
        int tryCount = 0;
        HttpRequestFactory requestFactory = (new NetHttpTransport()).createRequestFactory();

        try {
            HttpResponse httpResponse;
            do {
                HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));
                HttpHeaders headers = request.getHeaders();
                headers.setContentType("application/json");
                httpResponse = request.execute();
                getResponse = new HttpGetResponse(httpResponse.getStatusCode(), httpResponse.parseAsString());
                ++tryCount;
            } while (getResponse.getEntity().contains("success\":false") && tryCount < 10);

            httpResponse.disconnect();
        } catch (SocketTimeoutException var9) {
            //logs
        } catch (Exception var10) {
            //logs
        }

        return getResponse;
    }

    public static String getAsString(String url, String authToken, Boolean... ignoreWait) {
       String response=null;
        int tryCount = 0;
        HttpRequestFactory requestFactory = (new NetHttpTransport()).createRequestFactory();

        try {
            HttpResponse httpResponse;
            do {
                HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));
                HttpHeaders headers = request.getHeaders();
                headers.setContentType("application/json");
                httpResponse = request.execute();
            } while (!httpResponse.getStatusMessage().contains("OK") && tryCount < 10);


            response=httpResponse.parseAsString();
            httpResponse.disconnect();

        } catch (SocketTimeoutException var9) {
            //logs
        } catch (Exception var10) {
            //logs;
        }

        return response;
    }




        public static RestResponse post(String url, JSONObject entity) {
            HttpRequestFactory requestFactory = (new NetHttpTransport()).createRequestFactory();

            try {
                //checkSleep();
                HttpRequest request = requestFactory.buildPostRequest(new GenericUrl(url), ByteArrayContent.fromString("application/json", String.valueOf(entity)));
                HttpHeaders headers = request.getHeaders();
                HttpResponse httpResponse = request.execute();
                String responseString = httpResponse.parseAsString();
                int responseCode = httpResponse.getStatusCode();
                String responseStatus = httpResponse.getStatusMessage();
                RestResponse restResponse = new RestResponse(responseString, responseCode, responseStatus);
                return restResponse;
            } catch (SocketTimeoutException var11) {
                //TcLog.error("Rest call 'post' timed out!", new Exception[0]);
                return null;
            } catch (HttpResponseException var12) {
                //TcLog.error(var12.getContent(), new Exception[0]);
                return new RestResponse(var12.getContent(), var12.getStatusCode(), var12.getMessage());
            } catch (Exception var13) {
                //TcLog.error(var13.getMessage(), new Exception[0]);
                return null;
            }
        }

        public static RestResponse put(String url, JSONObject entity, String authToken) {
            HttpRequestFactory requestFactory = (new NetHttpTransport()).createRequestFactory();
            RestResponse restResponse = null;

            try {
                Thread.sleep(1000);
                HttpRequest request = requestFactory.buildPutRequest(new GenericUrl(url), ByteArrayContent.fromString("application/json", entity.toString()));
                HttpHeaders headers = request.getHeaders();
                headers.setContentType("application/json");
                HttpResponse httpResponse = request.execute();
                restResponse = new RestResponse(httpResponse.parseAsString(), httpResponse.getStatusCode(), httpResponse.getStatusMessage());
            } catch (HttpResponseException var8) {
                //TcLog.error(var8.getContent(), new Exception[0]);
                return new RestResponse(var8.getContent(), var8.getStatusCode(), var8.getMessage());
            } catch (Exception var9) {
                //TcLog.error(var9.getMessage(), new Exception[0]);
            }

            return restResponse;
        }

        public static RestResponse deleteWithBody(String url, JSONObject entity, String authToken) {
            HttpRequestFactory requestFactory = (new NetHttpTransport()).createRequestFactory();
            RestResponse restResponse = null;

            try {
                Thread.sleep(1000);
                HttpRequest request = requestFactory.buildRequest("DELETE", new GenericUrl(url), ByteArrayContent.fromString("application/json", entity.toString()));
                HttpHeaders headers = request.getHeaders();
                headers.setContentType("application/json");
                HttpResponse httpResponse = request.execute();
                restResponse = new RestResponse(httpResponse.parseAsString(), httpResponse.getStatusCode(), httpResponse.getStatusMessage());
            } catch (HttpResponseException var8) {
                //TcLog.error(var8.getMessage(), new Exception[0]);
                return new RestResponse(var8.getContent(), var8.getStatusCode(), var8.getMessage());
            } catch (Exception var9) {
                //TcLog.error(var9.getMessage(), new Exception[0]);
            }

            return restResponse;
        }

        public static RestResponse delete(String url, String authToken) {
            HttpRequestFactory requestFactory = (new NetHttpTransport()).createRequestFactory();
            RestResponse restResponse = null;

            try {
                Thread.sleep(1000);
                HttpRequest request = requestFactory.buildDeleteRequest(new GenericUrl(url));
                HttpHeaders headers = request.getHeaders();
                headers.setContentType("application/json");
                HttpResponse httpResponse = request.execute();
                restResponse = new RestResponse(httpResponse.parseAsString(), httpResponse.getStatusCode(), httpResponse.getStatusMessage());
            } catch (HttpResponseException var7) {
                //TcLog.error(var7.getMessage(), new Exception[0]);
                return new RestResponse(var7.getContent(), var7.getStatusCode(), var7.getMessage());
            } catch (Exception var8) {
                //TcLog.error(var8.getMessage(), new Exception[0]);
            }

            return restResponse;
        }

        public static String formDataPost(File file) throws IOException {
           try {
               HttpClient client = new MockHttpClient();
               HttpPost post = new HttpPost("http://54.169.34.162:5252/upload");
               FileBody fileBody = new FileBody(file, ContentType.DEFAULT_BINARY);
//
               MultipartEntityBuilder builder = MultipartEntityBuilder.create();
               builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
               builder.addPart("file", fileBody);
               HttpEntity entity = builder.build();
//
               post.setEntity(entity);
               org.apache.http.HttpResponse response = client.execute(post);
               return response.toString();
           }catch (Exception e){
               e.printStackTrace();
           }
           return null;
        }
}
