package com.testautothon.RestCall;

public class HttpGetResponse {

        private final int statusCode;
        private final String entity;

        public HttpGetResponse(int statusCode, String entity) {
            this.statusCode = statusCode;
            this.entity = entity;
        }

        public int getStatusCode() {
            return this.statusCode;
        }

        public String getEntity() {
            return this.entity;
        }

}
