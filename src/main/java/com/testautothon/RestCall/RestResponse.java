package com.testautothon.RestCall;
public class RestResponse {
    private String entity;
    private int statusCode;
    private String statusMessage;

    public RestResponse(String entity, int statusCode, String statusMessage) {
        this.entity = entity;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }

    public String getEntity() {
        return this.entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return this.statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }
}
