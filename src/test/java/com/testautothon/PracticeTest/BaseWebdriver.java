package com.testautothon.PracticeTest;

import com.testautothon.driver.DriverFactory;
import com.testautothon.driver.DriverManager;
import com.testautothon.pages.YouTubueHomePage;
import com.testautothon.reports.ExtentReport;
import com.testautothon.utils.PropertyManager;
import com.testautothon.utils.TestLogger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.io.IOException;

public class BaseWebdriver extends DriverFactory {
    WebDriver driver;
    TestLogger logger = new TestLogger();
    protected String serviceUrl = PropertyManager.getInstance().getURL();
    protected static final String browse= PropertyManager.getInstance().getBrowser();



    @AfterClass
    public void teardown(){
        if (this.driver != null) {
            try {
                this.driver.quit();
            } catch (Exception e) {
                // Driver maybe already closed. Ignore.
            }
        }
    }

    /* Before Class */
    @Parameters({ "browser" })
    @BeforeClass(alwaysRun = true)
    public void beforeClass(@Optional("") String browser) throws Exception {
        if(browser==null||browser=="") {
            browser = browse;
        }
        extent = ExtentReport.getExtent();
        if (DriverManager.getRunOn().equalsIgnoreCase("StandAlone")) {
            if (DriverManager.getType().equalsIgnoreCase("Desktop")) {
                driver = invokeBrowser(browser);
                /*webLoginPO = PageFactory.initElements(driver, WebLogin.class);
                webDashBoardPO = PageFactory.initElements(driver, WebDashboard.class);
                webUsersPO = PageFactory.initElements(driver, WebUsers.class);*/
            }
            else
            {
                driver = invokeBrowser(browser,"yes");
            }
        }
    }

    /* After Class */
    @AfterClass
    public void afterClass() throws IOException, InterruptedException {
        extent.flush();
        if (DriverManager.getType().equalsIgnoreCase("Device")) {
            //appiumStop();
        } else if (DriverManager.getType().equalsIgnoreCase("Desktop")) {
            closeBrowser();
        }
    }
}
