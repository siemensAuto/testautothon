package com.testautothon.PracticeTest;


import com.testautothon.driver.DriverFactory;
import com.testautothon.driver.DriverManager;
import com.testautothon.reports.ExtentReport;
import com.testautothon.utils.PropertyManager;
import org.apache.tools.ant.taskdefs.Parallel;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;
import com.testautothon.pages.HomePage;
import com.testautothon.pages.WikiPage;
import utils.ExcelDataReader;
import utils.PropertyUtil;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;

/*
public class BaseWebdriverTests extends DriverFactory {


    WebDriver driver;
    testautothon.utils.reporting.TestLogger logger = new testautothon.utils.reporting.TestLogger();
    protected String serviceUrl = PropertyManager.getInstance().getURL();
    protected String browse=PropertyManager.getInstance().getBrowser();

    public WikiPage openWikiHomePage(String movieName) {
        serviceUrl = "https://www.google.com";
        HomePage homePage = new HomePage(serviceUrl,driver);
        WikiPage wikiPage = homePage.openMovieWikiPage(movieName);
        return new WikiPage(this.driver);
    }

    @DataProvider (parallel = true)
    public static Object[][] ReadExcelData() {
        Object[][] obj = null;
        try {
            obj = ExcelDataReader.ReadExcelData();
        }catch (Exception e){
            e.printStackTrace();
        }
        return obj;
    }

    @AfterMethod(alwaysRun = true)
    public void afterMethodWebDriverTest(final Method method) {
        // Close driver if not null
        if (this.driver != null) {
            try {
                System.out.println("<Thread-id: " + Thread.currentThread().getId() + ">"
                        + " Ending test: " + method.getDeclaringClass().getName() + "."
                        + method.getName() + " On Date: " + new Date());
                //this.driver.quit();
            } catch (Exception e) {
                // Driver maybe already closed. Ignore.
            }
        }
    }

    @AfterClass
    public void teardown(){
        if (this.driver != null) {
            try {
                this.driver.quit();
            } catch (Exception e) {
                // Driver maybe already closed. Ignore.
            }
        }
    }

    */
/* Before Class *//*

    @Parameters({ "browser" })
    @BeforeClass(alwaysRun = true)
    public void beforeClass(String browser) throws Exception {
        extent = ExtentReport.getExtent();
        if (DriverManager.getRunOn().equalsIgnoreCase("grid")) {
            if (DriverManager.getType().equalsIgnoreCase("Desktop")) {
                driver = invokeBrowserInGrid(browser);
             */
/*   webLoginPO = PageFactory.initElements(driver, WebLogin.class);
                webDashBoardPO = PageFactory.initElements(driver,
                        WebDashboard.class);
                webUsersPO = PageFactory.initElements(driver, WebUsers.class);*//*

            } else if (DriverManager.getType().equalsIgnoreCase("Device")) {
                appiumStart();
                driver = setupInGrid(browser);
         */
/*       deviceLoginPO = PageFactory.initElements(driver,DeviceLogin.class);
                deviceDashBoardPO = PageFactory.initElements(driver,DeviceDashboard.class);*//*


            } else if (DriverManager.getType().equalsIgnoreCase("App")) {
                appiumStart();
                driver = setupApp(browser);
                */
/*appCreateNewFormPO = PageFactory.initElements(driver,
                        CreateForm.class);*//*

            }
        }

        else if (DriverManager.getRunOn().equalsIgnoreCase("StandAlone")) {
            if (DriverManager.getType().equalsIgnoreCase("Desktop")) {
                driver = invokeBrowser(browser);*/
/*
                webLoginPO = PageFactory.initElements(driver, WebLogin.class);
                webDashBoardPO = PageFactory.initElements(driver, WebDashboard.class);
                webUsersPO = PageFactory.initElements(driver, WebUsers.class);*//*

            } else if (DriverManager.getType().equalsIgnoreCase("Device")) {
                appiumStart();
                driver = setup(browser);
              */
/*  deviceLoginPO = PageFactory.initElements(driver, DeviceLogin.class);
                deviceDashBoardPO = PageFactory.initElements(driver, DeviceDashboard.class);*//*

            } else if (DriverManager.getType().equalsIgnoreCase("App")) {
                appiumStart();
                setupApp(browser);
                */
/*appCreateNewFormPO = PageFactory.initElements(driver,
                        CreateForm.class);*//*

            }
        }
    }

    */
/* After Class *//*

    @AfterClass
    public void afterClass() throws IOException, InterruptedException {
        extent.flush();
        if (DriverManager.getType().equalsIgnoreCase("Device")) {
            appiumStop();
        } else if (DriverManager.getType().equalsIgnoreCase("Desktop")) {
            closeBrowser();
        }
    }

*/

//}
