package com.testautothon.PracticeTest;

import com.testautothon.pages.YouTubeChannelPage;
import com.testautothon.pages.YouTubueHomePage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class StepInPageVerificationTest extends BaseWebdriver{

    @Test(timeOut = 50000, description = "Simple test")
    public void verifyStepInPage()
    {
        try{
            YouTubueHomePage youTubueHomePage = new YouTubueHomePage(serviceUrl,driver);
            Assert.assertTrue(youTubueHomePage.search("step-inform", true));

            YouTubeChannelPage youTubeChannelPage = youTubueHomePage.clickOnChannel();
            Assert.assertTrue(youTubeChannelPage.clickVideosTab(true));

            String videoName = youTubeChannelPage.getVideoName();
            youTubeChannelPage.scrollToCentre(videoName);
            Assert.assertNotNull(videoName);


        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("here");
        }

    }
}
