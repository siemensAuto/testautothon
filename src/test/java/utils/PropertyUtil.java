package utils;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyUtil {
    /**
     * instance object variable for properties.
     */
    private final Properties properties = new Properties();


    private static PropertyUtil instance;
    private static final Object lock = new Object();
    private static String propertyFilePath = System.getProperty("user.dir")+
            "\\src\\testData\\default.properties";
    private static String url;
    private static String wrongUsername;
    private static String wrongPassword;

    //Create a Singleton instance. We need only one instance of Property Manager.
    public static PropertyUtil getInstance () {
        if (instance == null) {
            synchronized (lock) {
                instance = new PropertyUtil();
                instance.loadData();
            }
        }
        return instance;
    }

    //Get all configuration data and assign to related fields.
    private void loadData() {

        //Read configuration.properties file
        try {
            properties.load(new FileInputStream(propertyFilePath));
            //prop.load(this.getClass().getClassLoader().getResourceAsStream("configuration.properties"));
        } catch (IOException e) {
            System.out.println("Configuration properties file cannot be found");
        }
    }

/*    public String getHubUrl() {
        return this.properties.getProperty("hub.url") + "/wd/hub";
    }*/

    /**
     * Method used to get value for particular property.
     *
     * @param property
     *            (property)
     * @return property value
     */
    public String getProperty(final String property) {
        return this.properties.getProperty(property);
    }

    /**
     * Method used to set value for particular property.
     */
    public void setProperty(final String key, final String value) {
        this.properties.setProperty(key, value);
    }
}
